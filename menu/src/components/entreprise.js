import React, { useState } from "react";
import PhoneInput from "react-phone-number-input";
import 'react-phone-number-input/style.css'
import '../App.css';
import facebook from '../logo_reseaux_sociaux/facebook.png';
import instagram from '../logo_reseaux_sociaux/instagram.png';
import linkedin from '../logo_reseaux_sociaux/linkedin.png';
import snapchat from '../logo_reseaux_sociaux/snapchat.png';
import twitter from '../logo_reseaux_sociaux/twitter.png';
import youtube from '../logo_reseaux_sociaux/youtube.png';
import { isValidPhoneNumber } from "react-phone-number-input";
import { formatPhoneNumber } from "react-phone-number-input";
import { formatPhoneNumberIntl } from "react-phone-number-input";
import { isPossiblePhoneNumber } from "react-phone-number-input";

export function Entreprise() {
    // Informations sur l'établissement
    const [entrepriseNom, setEntrepriseNom] = useState("")
    const [description, setDescription] = useState("")

    // Coordonnées
    const [email, setEmail] = useState("")
    const [siteWeb, setSiteWeb] = useState("")

    // Reseaux sociaux
    const [facebookName, setFacebookName] = useState("")
    const [instagramName, setInstagramName] = useState("")
    const [linkedinName, setLinkedinName] = useState("")
    const [snapchatName, setSnapchatName] = useState("")
    const [twitterName, setTwitterName] = useState("")
    const [youtubeName, setYoutubeName] = useState("")

    function handleClick() {
        // Informations sur l'établissement
        entrepriseNom === '' ? console.log('empty') : console.log(`Nom d l'établissement : ${entrepriseNom}`)
        description === '' ? console.log('empty') : console.log(`Description de l'établissement : ${description}`)

        // Coordonnées
        siteWeb === '' ? console.log('empty') : console.log(`Site Web : ${siteWeb}`)
        email === '' ? console.log('empty') : console.log(`Email : ${email}`)

        // Reseaux sociaux
        facebookName === '' ? console.log('empty') : console.log(`Facebook : ${facebookName}`)
        instagramName === '' ? console.log('empty') : console.log(`Instagram : ${instagramName}`)
        linkedinName === '' ? console.log('empty') : console.log(`Linkedin : ${linkedinName}`)
        snapchatName === '' ? console.log('empty') : console.log(`Snapchat : ${snapchatName}`)
        twitterName === '' ? console.log('empty') : console.log(`Twitter : ${twitterName}`)
        youtubeName === '' ? console.log('empty') : console.log(`Youtube : ${youtubeName}`)
    }

    const [value, setValue] = useState()

    return(
        <div>
            <nav className="nav">
               <button className="button"></button>
            </nav>
                <div className="column">
                    <div className="column_1">
                        <div className="row_1">
                            <h3>Informations sur l'établissement</h3>
                            <label>Nom de l'établissement</label><br/>
                            <input type="text" placeholder="Exemple: Gotham" onChange={(e) => setEntrepriseNom(e.target.value)} /><br/>
                            <label>Description de l'établissement</label><br/>
                            <input type="text" placeholder="Exemple : Bar, Restaurant" onChange={(e) => setDescription(e.target.value)} />

                        </div>
                        <div className="row_2">
                            <h3>Coordonnées</h3>
                            <label>Téléphone</label>
                            <div className="divPhoneInput">
                            <PhoneInput defaultCountry="FR" className="phoneInput" placeholder="Entrer numéro de téléphone" value={value} onChange={setValue} />
                            <p className="inputParagraph">{value && isValidPhoneNumber(value) ? "" : "Ce numéro ne correspond pas"}</p>
                            </div>
                            <label>Site Web</label><br/>
                            <input type="text" placeholder="Exemple : Gotham.com" onChange={(e) => setSiteWeb(e.target.value)} /><br/>
                            <label>Email</label><br/>
                            <input type="email" placeholder="Exemple : gotham@hotmail.fr" onChange={(e) => setEmail(e)} />
                        </div>
                    </div>
                    <div className="column_2">
                        <div className="row_3">
                            <h3>Emplacement</h3>
                            <iframe  src="https://api.maptiler.com/maps/basic-v2/?key=o84Zfu50Ec52pxvAwH7r#1.0/0.00000/0.00000"></iframe>
                        </div>

                        <div className="row_4">
                            <h3>Reseaux sociaux</h3>
                            <div>
                                <img alt="facebook" src={facebook}/>
                                <input placeholder="Facebook"
                                onChange={(e) => setFacebookName(e.target.value)}/><br/>
                            </div>
                            <div>
                                <img alt="instagram" src={instagram}/>
                                <input placeholder="Instagram"
                                onChange={(e) => setInstagramName(e.target.value)}/><br/>
                            </div>
                            <div>
                                <img alt="linkedin" src={linkedin}/>
                                <input placeholder="Linkedin"
                                onChange={(e) => setLinkedinName(e.target.value)} /><br/>
                            </div>
                            <div>
                                <img alt="snapchat" src={snapchat}/>
                                <input placeholder="Snapchat"
                                onChange={(e) => setSnapchatName(e.target.value)}/><br/>
                            </div>
                            <div>
                                <img alt="twitter" src={twitter}/>
                                <input placeholder="Twitter" 
                                onChange={(e) => setTwitterName(e.target.value)} /><br/>
                            </div>
                            <div>
                                <img alt="youtube" src={youtube}/>
                                <input placeholder="Youtube"
                                onChange={(e) => setYoutubeName(e.target.value)} /><br/>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div className="button_submit">
                   <button onClick={() => handleClick()} className="submitBtn">Enregistrer</button>
                </div>
        </div>
    )
}