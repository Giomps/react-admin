import { fontFamily } from "@mui/system";
import React from "react";
import '../App.css'

export function Modal4({closeModal4}) {

    const paragraphed = {
        display: 'flex',
        justifyContent: 'space-around'
    }

    const cancelled = {
        color: 'black',
        fontFamily: 'Poppins, sans-serif',
        cursor: 'pointer'
    }

    const deleted = {
        color: '#F7885D',
        fontFamily: 'Poppins, sans-serif',
        cursor: 'pointer'
    }

    return(
        <div className="modalBackground4">
            <div className="modalContainer4">
                <div className="modalHeader">
                </div>
                    <div className="indexed">
                        <h4 style={paragraphed}>Supprimer cette personne ?</h4>
                        <div style={paragraphed}>
                          <h4>La suppression de cette personne sera définitive</h4>
                        </div>
                    </div>
                    <div className="footer4">
                        <p style={cancelled} onClick={() => closeModal4(false)}>Annuler</p>
                        <p onClick={() => closeModal4(false)} style={deleted}>Supprimer</p>
                    </div>
                </div>
            </div>
    )
}