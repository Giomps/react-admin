import React from "react";
import { Link } from "react-router-dom";
import '../App.css'
import couverture from '../logo_galerie/map_1.png'
import profil from '../logo_galerie/map_2.png'

export function Galerie() {

    const galeries = [
        {img: couverture, titre: "Photo de couverture", photo: "1 photo"},
        {img: profil, titre: "Logo de profil", photo: "1 photo"},
        {img: profil, titre: "Galerie", photo: "10 photo"}
    ]

    return(
        <div className="galerie_style">
            <Link to={'/'}><button className="button"></button></Link>
            <h3>Galerie</h3>
            <p>Présenter votre établissement aux utilisateurs avec un visuel attrayant</p>
            <input type='file' />
            <h3>Vos photos</h3>
                {galeries.map((galerie) => (
                    <div className="card">
                        <img alt="picture" src={galerie.img}/>
                        <h3>{galerie.titre}</h3>
                        <p>{galerie.photo}</p>
                    </div>
                ))}
        </div>
    )
}