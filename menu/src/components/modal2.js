import React, { useState } from "react";
import '../App.css'
import wasted from '../logo_equipe/Waste.png'
import logo from '../logo_equipe/logo_1.png'
import icon from '../logo_equipe/icon.png'
import icon2 from '../logo_equipe/icon2.png'
import { Modal3 } from "./modal3";
import { Modal4 } from "./modal4";

export function Modal2({closeModal2}) {

    const [openModal3, setOpenModal3] = useState(false)
    const [openModal4, setOpenModal4] = useState(false)

    const index = {
        zIndex: '3',
        marginTop: '-31px',
        height: '60px'
    }

    const font = {
        fontFamily: "Montserrat"
    }

    const paragraphed = {
        display: 'flex',
        justifyContent: 'space-around'
    }

    const contact = {
        display: 'flex',
        justifyContent: 'space-between',
        fontFamily: "Montserrat",
        fontSize: '13px'
    }

    return(
        
        <div className="modalBackground2">
                   {openModal3 && <Modal3 closeModal3={setOpenModal3}/>}
                   {openModal4 && <Modal4 closeModal4={setOpenModal4}/>}
            <div className="modalContainer2">
                <div className="modalHeader">
                        <button onClick={() => closeModal2(false)} className="button"></button>
                        <img alt="" style={{cursor:'pointer'}} src={wasted} />
                </div>
                <div className="modalBody">
                    <div className="indexed">
                        <img style={index} src={logo} alt="4"/> 
                        <h3 style={font}>Stéphane Jacquet</h3>
                        <div style={paragraphed}>
                            <p>Rôle</p>
                            <p>Date</p>
                        </div>
                        <div style={contact}>
                            <p>Numéro de téléphone</p>
                            <img height={30} src={icon} alt='icon'/>
                        </div>
                        <div style={contact}>
                            <p>Email</p>
                            <img height={25} src={icon2} alt='icon' />
                        </div>
                    </div>
                    <div className="footer2">
                    <div>
                        <button className="updateBtn" onClick={() => setOpenModal3(true)}>Modifier le rôle</button>
                    </div>
                    <div>
                        <button onClick={() => setOpenModal4(true)} className="suppress">Supprimer</button>
                    </div>
                </div>
                </div>
            </div>
        </div>
    )
}