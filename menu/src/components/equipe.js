import React, { useState } from "react";
import { Link } from "react-router-dom";
import '../App.css'
import add from '../logo_equipe/add.png'
import { Modal } from "./modal";
import logo_1 from '../logo_equipe/logo_1.png'
import logo_2 from '../logo_equipe/logo_2.png'
import logo_3 from '../logo_equipe/logo_3.png'
import logo_4 from '../logo_equipe/logo_4.png'
import logo_5 from '../logo_equipe/logo_5.png'
import { Modal2 } from "./modal2";

export function Equipe() {

    // const teams = [
    //     {id:1, name: "Timothy", member:'Propriétaire', date: "05/12/2021", img:logo_1},
    //     {id:2, name: "Annabel", member:'Administateur', date: "01/01/2022",
    //     img:logo_2},
    //     {id:3, name: "Reyna", member:'Éditeur', date: "05/03/2022", 
    //     img:logo_3},
    //     {id:4, name: "Stéphane", member:'Éditeur', date: "07/07/2022", img:logo_4},
    //     {id:5, name: "Benoit", member:'Éditeur', date: "En attente", img:logo_5},
    // ]

    const [openModal, setOpenModal] = useState(false)
    const [openModal2, setOpenModal2] = useState(false)
    const [openModal3, setOpenModal3] = useState(false)

    return(
        <div className="container">
           {openModal && <Modal closeModal={setOpenModal}/>}
           {openModal2 && <Modal2 closeModal2={setOpenModal2}/>}
            <div className="row">
                <nav className="nav">
                   <button className="button"></button>
                    <img onClick={() => setOpenModal(true)} style={{cursor:'pointer'}} src={add}  />
                </nav>
                <h3>Gestion d'équipe</h3>
                <div className="row_team">
                        <div className="map">
                            <img src={logo_1} alt="logo"/>
                            <div className="name">
                                <h4>Timothy</h4>
                                <p>Editeur</p>
                            </div>
                            <div className="date">
                                <h3>05/03/2022</h3>
                            </div>
                            <div className="div_chevron">
                                <i onClick={() => setOpenModal2(true)} className="chevron"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    )
}