import React from "react";
import '../App.css'
import wasted from '../logo_equipe/Waste.png'
import logo from '../logo_equipe/logo_1.png'
import icon from '../logo_equipe/icon.png'
import icon2 from '../logo_equipe/icon2.png'


export function Modal3({closeModal3}) {

    const index = {
        zIndex: '3',
        marginTop: '-31px',
        height: '60px'
    }

    const font = {
        fontFamily: "Montserrat"
    }

    const paragraphed = {
        display: 'flex',
        justifyContent: 'space-around'
    }

    const contact = {
        display: 'flex',
        justifyContent: 'space-between',
        fontFamily: "Montserrat",
        fontSize: '13px'
    }

    return(
        <div className="modalBackground2">
            <div className="modalContainer2">
                <div className="modalHeader">
                        <button onClick={() => closeModal3(false)} className="button"></button>
                        <img alt="" style={{cursor:'pointer'}} src={wasted} />
                </div>
                <div className="modalBody">
                    <div className="indexed">
                        <img style={index} src={logo} alt="4"/> 
                        <h3 style={font}>Stéphane Jacquet</h3>
                        <div style={paragraphed}>
                            <p>Rôle</p>
                            <p>Date</p>
                        </div>
                        <div>
                            <ul>
                                <li>vvv</li>
                                <li>ddd</li>
                                <li>ffff</li>
                            </ul>
                        </div>
                        <div style={contact}>
                            <p>Numéro de téléphone</p>
                            <img height={30} src={icon} alt='icon'/>
                        </div>
                        <div style={contact}>
                            <p>Email</p>
                            <img height={25} src={icon2} alt='icon' />
                        </div>
                    </div>
                    <div className="footer3">
                        <button onClick={() => closeModal3(false)} className="modal3btn">Enregistrer</button>
                    </div>
                </div>
            </div>
        </div>
    )
}