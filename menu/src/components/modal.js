import React, { useState } from "react";
import '../App.css';
import { Link } from "react-router-dom";
import { style } from "@mui/system";

export function Modal({closeModal}) {
    const [email, setEmail] = useState(null)

    const button = {
        backgroundColor: 'white',
        border: '2px solid #F7885D',
        borderRadius: '30px',
        height: '35px',
        width: '35px'
    }

    const clickedButton = {
        backgroundColor: '#f7885D',
        border: '2px solid #F7885D',
        borderRadius: '30px',
        height: '35px',
        width: '35px'
    }

    return(
        <div className="modalBackground">
            <div className="modalContainer">
                <div className="titleCloseBtn">
                </div>
                <div className="h1_modal">
                    <h1>Ajouter des nouveaux membres</h1>
                </div>
                <input className="input_modal" placeholder="Entrer une adresse email..." type='text' onChange={(e) => setEmail(e.target.value)}/><br/>
                <div className="body">
                    <div className="flex">
                        <button value="Editeur" style={button}></button> 
                        <div>
                            <p>Editeur</p>
                            <p>Et ipsa similique eum voluptas aperiam est suscipit suscipit quo dignissimos adipisci.</p>
                        </div> 
                    </div>
                </div>
                <div className="footer">
                    <button id="cancelBtn" onClick={() => closeModal(false)}>Annuler</button>
                    <button>Ajouter</button>
                </div>
            </div>
        </div>
    )
}